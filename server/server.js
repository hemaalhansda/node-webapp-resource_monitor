const path = require('path');
const http = require('http');
const express = require('express');
const socketIO = require('socket.io');
const usage = require('usage');
const hbs = require('hbs');
const fs = require('fs');

const publicPath = path.join(__dirname, '../public');
const port = process.env.PORT || 3000
app.use(express.static(publicPath));


var app = express();
var server = http.createServer(app);
var io = socketIO(server);

app.set('view engine', 'hbs');

var pid = process.pid;
var memory;
var cpuUsage;
usage.lookup(pid, function (err, res) {
  var mem = res.memory;
  var memMb = (mem/1000)/1000;
  var cpu = res.cpu;
  memory = memMb;
  cpuUsage =cpu;
  console.log(memMb + ' MB');
  console.log(cpu + '%');
});



app.get('/', (req, res) => {
  res.render('index.hbs' {
    cpu: cpuUsagem,
    memory: memory
  });
});

server.listen(port, () => {
  console.log(`Started up at port ${port}`);
});
